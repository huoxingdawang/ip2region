# db maker C implementation

### 1, How to build ?

### 2, How to make ?

comming soon ...

以下内容由[火星大王](https://gitee.com/huoxingdawang)在2020年8月19日更新

[我](https://gitee.com/huoxingdawang)尝试在我自己的项目里集成了ip2region，并且支持了C的database更新

使用方法：

1.在当前目录下make init

2.在当前目录下make

3.运行ip2region exes\ip2region

3.1.输入ip 返回region

3.2.直接敲回车退出

3.3. **[我](https://gitee.com/huoxingdawang)只写了memeory算法**

4.更新数据库

4.1. 数据源来自..//..//data//ip.merge.txt **不能有末尾回车**

4.2. 更新会直接覆盖..//..//data//ip2region.db，具体输出位置在examples//ip2region_maker.c 的21行

4.3. **请务必保证数据源的ip不重叠，递增，否则二分查询会出大问题**
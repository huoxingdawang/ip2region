@echo off
REM xcopy %libs%\jbl jbl\ /E /Y /Q /R
REM attrib +r jbl\*
REM attrib -r jbl\jbl*config.h
REM xcopy %libs%\jwl jwl\ /E /Y /Q /R
REM attrib +r jwl\*
REM attrib -r jwl\jwl*config.h
copy examples\makefile + jbl\makefile + jwl\makefile makefile
